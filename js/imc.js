// Obtener el objeto botón de calcular //

const IMC = document.getElementById('IMC');
IMC.addEventListener('click', function(){
    let altura = parseFloat(document.getElementById('txtAltura').value);
    let peso = document.getElementById('txtPeso').value;

    // Realizar cálculos
    let imc = peso / (altura ** 2);
    let resultado = imc;

    // Mostrar datos
    document.getElementById('txtResultado').value = resultado.toFixed(2);
    // Mostrar el label invisible
    document.getElementById('resultado').style.display = "block";
    document.getElementById('txtResultado').style.display = "block";

    // Comparación del IMC ideal //
    if (resultado >= 18.5 && resultado <= 24.9) {
        txtResultado.style.color = "green";
        document.getElementById('resultado').style.color = "green";
    } else {
        txtResultado.style.color = "red";
        document.getElementById('resultado').style.color = "red";
    }

    IMCimage(resultado, peso);
});

function IMCimage(imc, peso){
    let edad = parseInt(document.getElementById('txtEdad').value);
    let imagen = "";
    let genHombre = document.getElementById('hombre').checked;
    let genMujer = document.getElementById('mujer').checked;
    let imagenElement = document.getElementById('imagen');
    let calorias = 0;
    if (genHombre) {
        if (imc < 18.5) {
            imagen = "01H.png";
        } else if (imc >= 18.5 && imc <= 24.99) {
            imagen = "02H.png";
        } else if (imc >= 25 && imc <= 29.99) {
            imagen = "03H.png";
        } else if (imc >= 30 && imc <= 34.99) {
            imagen = "04H.png";
        } else if (imc >= 35 && imc <= 39.99) {
            imagen = "05H.png";
        } else {
            imagen = "06H.png";
        }
    }

    if (genMujer) {
        if (imc < 18.5) {
            imagen = "01M.png";
        } else if (imc >= 18.5 && imc <= 24.99) {
            imagen = "02M.png";
        } else if (imc >= 25 && imc <= 29.99) {
            imagen = "03M.png";
        } else if (imc >= 30 && imc <= 34.99) {
            imagen = "04M.png";
        } else if (imc >= 35 && imc <= 39.99) {
            imagen = "05M.png";
        } else {
            imagen = "06M.png";
        }
    }
    
    verImagen(imagen);

    if(genHombre){
        if(edad >= 10 && edad <= 17){
            calorias = (17.686 * peso) + 658.2;
        } else if(edad >= 18 && edad <= 29){
            calorias = (15.057 * peso) + 692.2;
        } else if(edad >= 30 && edad <=59){
            calorias = (11.472 * peso) + 873.1;
        } else{
            calorias = (11.711 * peso) + 587.7;
        }
    }

    if(genMujer){
        if(edad >= 10 && edad <= 17){
            calorias = (13.384 * peso) + 692.6;
        } else if(edad >= 18 && edad <= 29){
            calorias = (14.818 * peso) + 486.6;
        } else if(edad >= 30 && edad <=59){
            calorias = (8.126 * peso) + 845.6;
        } else{
            calorias = (9.082 * peso) + 658.5;
        }
    }

    document.getElementById('txtCalorias').value = calorias.toFixed(0);

}

function verImagen(imagen){
    let imgSrc = "/img/" + imagen;
    let imagenElement = document.getElementById('imagen');
    imagenElement.src = imgSrc;
}





