// Obtener el objeto botón de calcular //

const btnCalcular = document.getElementById('btnCalcular');
btnCalcular.addEventListener('click', function(){
    let valorAuto = document.getElementById('txtValorAuto').value;
    let porPagoInicial = document.getElementById('txtPorcentaje').value;
    let plazo = document.getElementById('plazos').value;

    // Realizar cálculos
    let pagoInicial = valorAuto * (porPagoInicial/100);
    let totalFin = valorAuto - pagoInicial;
    let plazos = totalFin / plazo;

    // Mostrar datos
    document.getElementById('txtPagoInicial').value = pagoInicial;
    document.getElementById('txtTotalFin').value = totalFin;
    document.getElementById('txtPagoMensual').value = plazos.toFixed(2);
});

// Codificar el botón de limpiar

btnLimpiar.addEventListener('click', function(){
    document.getElementById('txtValorAuto').value = "";
    document.getElementById('txtPorcentaje').value = "";
    document.getElementById('txtPagoInicial').value = "";
    document.getElementById('txtTotalFin').value = "";
    document.getElementById('txtPagoMensual').value = "";
});