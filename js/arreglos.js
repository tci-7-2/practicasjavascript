// Generación de números aleatorios con arreglos y funciones
// Declaraciónd del arreglo
let array = [];

document.getElementById('gen').addEventListener('click', genNumerosAleatorios);
function genNumerosAleatorios(){
    // Limpiar arreglo
    array = [];

    let cantidad = parseInt(document.getElementById('gennum').value);
    // Generación de números aleatorios entre 0 y 999
    for(let i = 0; i<cantidad; i++){
        let numAleatorio = Math.floor(Math.random() * 1000);
        array.push(numAleatorio);
    }
    
    // Mostrar los números aleatorios en el select
    const cmbAleatorios = document.getElementById('cmbNumeros');
    cmbAleatorios.innerHTML = '';
    
    for(let i = 0; i < array.length; i++){
        let option = document.createElement('option');
        option.value = array[i];
        option.innerHTML = array[i];
        cmbAleatorios.appendChild(option);
    }

    // Llamada a la función para calcular el promedio de los números aleatorios
    let prom = calcularProm(array);

    // Mostrar el promedio en el label Promedio del HTML
    let promLabel = document.getElementById('promValor');
    promLabel.textContent = prom.toFixed(0);

    // Llamada a la función para sacar el mayor y el menor de los números aleatorios
    let { mayorValor, posicionMayor } = mayorPos(array);
    let { menorValor, posicionMenor } = menorPos(array);

    // Mostrar el mayor y menor de los numeros aleatorios del HTML 
    // separados en dos label's
    let mayorLabel = document.getElementById('mayorValor');
    mayorLabel.textContent = `Mayor: ${mayorValor}, Índice: ${posicionMayor}`;
    let menorLabel = document.getElementById('menorValor');
    menorLabel.textContent = `Menor: ${menorValor}, Índice: ${posicionMenor}`;

    // Llamada a la función para sacar el porcentaje de pares y 
    // Inicializar impares para sacar el porcentaje de impares
    let porPares = simetrico(array);
    let porImpares = 100 - porPares;
    
    // Mostrar los porcentajes de pares e impares en el html
    let porParesLabel = document.getElementById('porPares');
    porParesLabel.textContent = `: ${porPares.toFixed(0)}%`;
    let porImparesLabel = document.getElementById('porImpares');
    porImparesLabel.textContent = `: ${porImpares.toFixed(0)}%`;

    // Verificar si es simetrico o no el arreglo
    let simYN = document.getElementById('simYN');

    if(porPares >= porImpares + 20){
        simYN.textContent = "Sí";
    } else {
        simYN.textContent = "No";
    }
}

// Función para calcular el promedio y que muestre ese promedio
function calcularProm(val){
    if(val.length == 0){
        return 0;
    }
    let sum = val.reduce((a,b) => a + b, 0);
    return sum / val.length;
}

// Función que regresa el valor mayor del contenido array y su posición //
function mayorPos(val){
    if(val.length == 0){
        return { mayorValor: 0, posicionMayor: -1 };
    }
    let mayor = val[0];
    let posicion = 0;
    for(let i = 1; i<val.length; i++){
        if(val[i] > mayor){
            mayor = val[i];
            posicion = i;
        }
    }
    return { mayorValor: mayor, posicionMayor: posicion };
}
// Función que regresa el valor menor del contenido array y su posición
function menorPos(val){
    if(val.length == 0){
        return { menorValor: 0, posicionMenor: -1 };
    }
    let menor = val[0];
    let posicion = 0;
    for(let i = 1; i<val.length; i++){
        if(val[i] < menor){
            menor = val[i];
            posicion = i;
        }
    }
    return { menorValor: menor, posicionMenor: posicion };
}

// Función para calcular el porcentaje de números pares del arreglo
function simetrico(arreglo){
    let numPares = arreglo.filter(numero => numero % 2 == 0);
    return (numPares.length / arreglo.length) * 100;
}