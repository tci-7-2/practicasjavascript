const btnMostrar = document.getElementById('btnMostrar');
btnMostrar.addEventListener('click',function(){
    var opt = document.querySelectorAll('input[name="tablaMul"]');
    let num = document.getElementById('opt').value;
    verImagen(num);
    for(let i = 1; i <= 10; i++){
        multi(num, i);
    }
});

function multi(a, b){
    var res = a * b;
    var num1 = Math.floor(res/10);
    var num2 = res % 10;
    document.getElementById('num1'+b).src = "/img/" + num1 + ".png";
    document.getElementById('num2'+b).src = "/img/" + num2 + ".png";
}

function verImagen(a){
    var img = document.querySelectorAll("[id='numsel'");
    for(let ims of img){
        ims.src = "/img/" + a + ".png";
    }
}